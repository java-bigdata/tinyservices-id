package com.ctw.tinyservices.id.portal.utils;

/**
 * @author TongWei.Chen 2021/12/16 15:35
 *
 * TimeUtil
 **/
public class TimeUtils {
    private TimeUtils() {
        throw new RuntimeException("工具类，直接用，别瞎new");
    }

    public static final String formatDateAgo(long millisecond) {
        StringBuilder sb = new StringBuilder();
        if (millisecond < 1000L) {
            return sb.append(millisecond).append("毫秒").toString();
        }
        int ss = 1000;
        int mi = ss * 60;
        int hh = mi * 60;
        int dd = hh * 24;
        long day = millisecond / (long)dd;
        long hour = (millisecond - day * (long)dd) / (long)hh;
        long minute = (millisecond - day * (long)dd - hour * (long)hh) / (long)mi;
        long second = (millisecond - day * (long)dd - hour * (long)hh - minute * (long)mi) / (long)ss;
        if (day > 0L) {
            sb.append(day).append("天");
        }

        if (hour > 0L) {
            sb.append(hour).append("时");
        }

        if (minute > 0L) {
            sb.append(minute).append("分");
        }

        if (second > 0L) {
            sb.append(second).append("秒");
        }

        return sb.toString();
    }
}