package com.ctw.tinyservices.id.portal.entity.segment.param;

import com.ctw.tinyservices.id.portal.entity.PageParam;

/**
 * @author TongWei.Chen 2022/4/22 13:33
 *
 * id_segment
 **/
public class IdSegmentListParam extends PageParam {
    /**
     * 业务类型，此token可访问的业务类型标识，全局唯一
     */
    private String bizType;

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IdSegmentListParam{");
        sb.append("bizType='").append(bizType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}