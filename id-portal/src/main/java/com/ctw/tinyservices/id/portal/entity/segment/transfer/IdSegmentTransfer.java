package com.ctw.tinyservices.id.portal.entity.segment.transfer;

import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.entity.IdSegment;
import com.ctw.tinyservices.id.portal.entity.segment.param.IdSegmentParam;
import com.ctw.tinyservices.id.portal.entity.segment.vo.IdSegmentVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author TongWei.Chen 2022/4/22 13:36
 *
 * id_segment mapstruct
 **/
@Mapper
public interface IdSegmentTransfer {
    IdSegmentTransfer INSTANCE = Mappers.getMapper(IdSegmentTransfer.class);

    IdSegment paramToPO(IdSegmentParam param);

    IdSegmentVO poToVO(IdSegment idSegment);

    PageResponseDTO<IdSegmentVO> poListToVOList(PageResponseDTO<IdSegment> idSegmentList);
}
