package com.ctw.tinyservices.id.portal.utils;

/**
 * @author TongWei.Chen 2021/12/16 15:35
 *
 * ByteUtil
 **/
public class ByteUtils {
    private ByteUtils() {
        throw new RuntimeException("工具类，直接用，别瞎new");
    }

    private static final int UNIT = 1024;

    public static final String formatByteSize(long byteSize) {
        if (byteSize <= -1L) {
            return String.valueOf(byteSize);
        } else {
            double size = 1.0D * (double)byteSize;
            String type = "B";
            if ((int)Math.floor(size / 1024.0D) <= 0) {
                type = "B";
                return format(size, type);
            } else {
                size /= 1024.0D;
                if ((int)Math.floor(size / 1024.0D) <= 0) {
                    type = "KB";
                    return format(size, type);
                } else {
                    size /= 1024.0D;
                    if ((int)Math.floor(size / 1024.0D) <= 0) {
                        type = "MB";
                        return format(size, type);
                    } else {
                        size /= 1024.0D;
                        if ((int)Math.floor(size / 1024.0D) <= 0) {
                            type = "GB";
                            return format(size, type);
                        } else {
                            size /= 1024.0D;
                            if ((int)Math.floor(size / 1024.0D) <= 0) {
                                type = "TB";
                                return format(size, type);
                            } else {
                                size /= 1024.0D;
                                if ((int)Math.floor(size / 1024.0D) <= 0) {
                                    type = "PB";
                                    return format(size, type);
                                } else {
                                    return ">PB";
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static String format(double size, String type) {
        byte precision;
        if (size * 1000.0D % 10.0D > 0.0D) {
            precision = 3;
        } else if (size * 100.0D % 10.0D > 0.0D) {
            precision = 2;
        } else if (size * 10.0D % 10.0D > 0.0D) {
            precision = 1;
        } else {
            precision = 0;
        }

        String formatStr = "%." + precision + "f";
        if ("KB".equals(type)) {
            return String.format(formatStr, size) + "KB";
        } else if ("MB".equals(type)) {
            return String.format(formatStr, size) + "MB";
        } else if ("GB".equals(type)) {
            return String.format(formatStr, size) + "GB";
        } else if ("TB".equals(type)) {
            return String.format(formatStr, size) + "TB";
        } else {
            return "PB".equals(type) ? String.format(formatStr, size) + "PB" : String.format(formatStr, size) + "B";
        }
    }
}
