package com.ctw.tinyservices.id.portal.config;

import com.ctw.tinyservices.id.common.config.DataSourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

/**
 * @author TongWei.Chen 2022/4/18 13:23
 **/
@Configuration
public class JdbcConfig {

    @Bean
    public DataSource druidDataSource() {
        return DataSourceConfig.getInstance().getDruidDataSource();
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(druidDataSource());
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(druidDataSource());
    }

    @Bean(name = "txTemplate")
    public TransactionTemplate transactionTemplate (){
        return new TransactionTemplate(transactionManager());
    }

}
