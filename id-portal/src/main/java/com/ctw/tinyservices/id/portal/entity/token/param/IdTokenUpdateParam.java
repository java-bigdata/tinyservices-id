package com.ctw.tinyservices.id.portal.entity.token.param;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author TongWei.Chen 2022/4/18 17:24
 *
 * id_token
 **/
public class IdTokenUpdateParam {
    /**
     * 主键自增id
     */
    @NotNull(message = "id不能为空")
    @Min(value = 1, message = "id不合法")
    private Integer id;

    /**
     * 应用名
     */
    @NotBlank(message = "应用名不能为空")
    private String bizName;

    /**
     * token
     */
    @NotBlank(message = "token不能为空")
    private String token;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空")
    private String deptName;

    /**
     * 负责人：中文名称
     */
    @NotBlank(message = "负责人不能为空")
    private String ownerName;

    /**
     * 负责人邮箱
     */
    @NotBlank(message = "负责人邮箱不能为空")
    private String ownerEmail;

    /**
     * 备注，描述
     */
    private String remark;

    /**
     * 删除状态; 1: disabled, 0: enable
     */
    @Min(value = 0, message = "isDisabled参数不合法")
    @Max(value = 1, message = "isDisabled参数不合法")
    private int isDisabled;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizName() {
        return bizName;
    }

    public void setBizName(String bizName) {
        this.bizName = bizName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(int isDisabled) {
        this.isDisabled = isDisabled;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IdTokenParam{");
        sb.append("id='").append(id).append('\'');
        sb.append(", bizName='").append(bizName).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append(", deptName='").append(deptName).append('\'');
        sb.append(", ownerName='").append(ownerName).append('\'');
        sb.append(", ownerEmail='").append(ownerEmail).append('\'');
        sb.append(", remark='").append(remark).append('\'');
        sb.append(", isDisabled=").append(isDisabled);
        sb.append('}');
        return sb.toString();
    }
}
