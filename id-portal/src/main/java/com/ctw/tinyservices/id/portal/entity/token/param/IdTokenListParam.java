package com.ctw.tinyservices.id.portal.entity.token.param;

import com.ctw.tinyservices.id.portal.entity.PageParam;

/**
 * @author TongWei.Chen  2022/4/18 17:24
 *
 * id_token
 **/
public class IdTokenListParam extends PageParam {
    /**
     * 业务类型，此token可访问的业务类型标识，全局唯一
     */
    private String bizType;

    /**
     * 删除状态; 1: disabled, 0: enable
     */
    private Integer isDisabled;

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public Integer getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(Integer isDisabled) {
        this.isDisabled = isDisabled;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IdTokenUpdateParam{");
        sb.append("bizType='").append(bizType).append('\'');
        sb.append(", isDisabled=").append(isDisabled);
        sb.append('}');
        return sb.toString();
    }
}
