package com.ctw.tinyservices.id.portal.dao.segment;

import com.ctw.tinyservices.id.common.dao.BaseDAO;
import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.entity.IdSegment;
import com.ctw.tinyservices.id.portal.entity.segment.param.IdSegmentListParam;

import java.util.List;

/**
 * @author TongWei.Chen 2022/4/22 13:15
 *
 * id_segment
 **/
public interface IdSegmentDAO extends BaseDAO<IdSegment> {

    PageResponseDTO<IdSegment> listPage(IdSegmentListParam param);

    List<String> listBizType();

}
