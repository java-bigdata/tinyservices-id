package com.ctw.tinyservices.id.portal.log;

import java.lang.annotation.*;

/**
 * @author TongWei.Chen 2022/04/21 20:05
 *
 * IgnoreField Annotation
 **/
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreField {
    String[] value() default {};
}
