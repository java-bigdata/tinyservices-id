package com.ctw.tinyservices.id.portal.entity;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author TongWei.Chen  2022/4/18 19:53
 **/
public class PageParam {
    @Min(value = 1, message = "当前页数必须大于0")
    private int pageNo;
    @Min(value = 1, message = "每页条数必须大于0")
    @Max(value = 100, message = "每页条数不能大于100")
    private int pageSize;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PageParam{");
        sb.append("pageNo=").append(pageNo);
        sb.append(", pageSize=").append(pageSize);
        sb.append('}');
        return sb.toString();
    }
}
