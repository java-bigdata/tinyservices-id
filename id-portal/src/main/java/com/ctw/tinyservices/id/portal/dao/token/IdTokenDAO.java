package com.ctw.tinyservices.id.portal.dao.token;

import com.ctw.tinyservices.id.common.dao.BaseDAO;
import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.entity.IdToken;
import com.ctw.tinyservices.id.portal.entity.token.param.IdTokenListParam;

import java.util.List;

/**
 * @author TongWei.Chen 2022/4/18 13:15
 *
 * id_token
 **/
public interface IdTokenDAO extends BaseDAO<IdToken> {

    PageResponseDTO<IdToken> listPage(IdTokenListParam param);

    List<String> listBizType();

    void updateDisabled(Integer id, int isDisabled);
}
