package com.ctw.tinyservices.id.portal.api;

import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.dto.ResponseDTO;
import com.ctw.tinyservices.id.common.dto.helper.ResponseDTOHelper;
import com.ctw.tinyservices.id.common.exception.IllegalOperationException;
import com.ctw.tinyservices.id.portal.entity.token.param.IdTokenListParam;
import com.ctw.tinyservices.id.portal.entity.token.param.IdTokenParam;
import com.ctw.tinyservices.id.portal.entity.token.param.IdTokenUpdateParam;
import com.ctw.tinyservices.id.portal.entity.token.transfer.IdTokenTransfer;
import com.ctw.tinyservices.id.portal.entity.token.vo.IdTokenVO;
import com.ctw.tinyservices.id.portal.log.ApiLog;
import com.ctw.tinyservices.id.portal.service.IdTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author TongWei.Chen 2022/4/18 13:06
 *
 * id_token
 **/
@ApiLog
@RestController
@RequestMapping("/token")
public class IdTokenApi {

    @Autowired
    private IdTokenService idTokenService;

    @PostMapping("/save")
    public ResponseDTO<Void> save(@RequestBody @Valid IdTokenParam idTokenParam) {
        idTokenService.save(IdTokenTransfer.INSTANCE.paramToPO(idTokenParam));
        return ResponseDTOHelper.success();
    }

    @PutMapping("/update")
    public ResponseDTO<Void> update(@RequestBody @Valid IdTokenUpdateParam idTokenParam) {
        idTokenService.update(IdTokenTransfer.INSTANCE.updateParamToPO(idTokenParam));
        return ResponseDTOHelper.success();
    }

    @PutMapping("/updateStatus")
    public ResponseDTO<Void> updateStatus(Integer id, Integer status) {
        if (null == id || id < 1) throw new IllegalOperationException("id不合法");
        if (null == status || status < 0 || status > 1) throw new IllegalOperationException("非法操作");

        idTokenService.updateDisabled(id, status);
        return ResponseDTOHelper.success();
    }

    @GetMapping("/detail/{id}")
    public ResponseDTO<IdTokenVO> detail(@PathVariable Long id) {
        return ResponseDTOHelper.success(IdTokenTransfer.INSTANCE.poToVO(idTokenService.get(id)));
    }

    @GetMapping("/listPage")
    public ResponseDTO<PageResponseDTO<IdTokenVO>> listPage(IdTokenListParam param) {
        return ResponseDTOHelper.success(IdTokenTransfer.INSTANCE.poListToVOList(idTokenService.listPage(param)));
    }

    @GetMapping("/listBizType")
    public ResponseDTO<List<String>> listBizType() {
        return ResponseDTOHelper.success(idTokenService.listBizType());
    }
}
