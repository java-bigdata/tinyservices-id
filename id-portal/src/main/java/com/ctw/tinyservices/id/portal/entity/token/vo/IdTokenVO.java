package com.ctw.tinyservices.id.portal.entity.token.vo;

import java.util.Date;

/**
 * @author TongWei.Chen 2022/4/18 17:24
 *
 * id_token
 **/
public class IdTokenVO {
    /**
     * 主键自增id
     */
    private Integer id;

    /**
     * 业务类型，此token可访问的业务类型标识，全局唯一
     */
    private String bizType;

    /**
     * 应用名
     */
    private String bizName;

    /**
     * token
     */
    private String token;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 负责人：中文名称
     */
    private String ownerName;

    /**
     * 负责人：邮箱
     */
    private String ownerEmail;

    /**
     * 备注，描述
     */
    private String remark;

    /**
     * 删除状态; 1: disabled, 0: enable
     */
    private int isDisabled;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getBizName() {
        return bizName;
    }

    public void setBizName(String bizName) {
        this.bizName = bizName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(int isDisabled) {
        this.isDisabled = isDisabled;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IdToken{");
        sb.append("id=").append(id);
        sb.append(", bizType='").append(bizType).append('\'');
        sb.append(", bizName='").append(bizName).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append(", deptName='").append(deptName).append('\'');
        sb.append(", ownerName='").append(ownerName).append('\'');
        sb.append(", ownerEmail='").append(ownerEmail).append('\'');
        sb.append(", remark='").append(remark).append('\'');
        sb.append(", isDisabled=").append(isDisabled);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append('}');
        return sb.toString();
    }
}
