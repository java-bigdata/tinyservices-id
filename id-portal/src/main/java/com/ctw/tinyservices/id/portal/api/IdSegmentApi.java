package com.ctw.tinyservices.id.portal.api;

import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.dto.ResponseDTO;
import com.ctw.tinyservices.id.common.dto.helper.ResponseDTOHelper;
import com.ctw.tinyservices.id.portal.entity.segment.param.IdSegmentListParam;
import com.ctw.tinyservices.id.portal.entity.segment.param.IdSegmentParam;
import com.ctw.tinyservices.id.portal.entity.segment.transfer.IdSegmentTransfer;
import com.ctw.tinyservices.id.portal.entity.segment.vo.IdSegmentVO;
import com.ctw.tinyservices.id.portal.log.ApiLog;
import com.ctw.tinyservices.id.portal.service.IdSegmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author TongWei.Chen 2022/4/21 19:54
 *
 * id_segment
 **/
@ApiLog
@RestController
@RequestMapping("/segment")
public class IdSegmentApi {

    @Autowired
    private IdSegmentService idSegmentService;

    @PostMapping("/save")
    public ResponseDTO<Void> save(@RequestBody @Valid IdSegmentParam idSegmentParam) {
        idSegmentService.save(IdSegmentTransfer.INSTANCE.paramToPO(idSegmentParam));
        return ResponseDTOHelper.success();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseDTO<Void> updateStatus(@PathVariable Long id) {
        idSegmentService.delete(id);
        return ResponseDTOHelper.success();
    }

    @GetMapping("/detail/{id}")
    public ResponseDTO<IdSegmentVO> detail(@PathVariable Long id) {
        return ResponseDTOHelper.success(IdSegmentTransfer.INSTANCE.poToVO(idSegmentService.get(id)));
    }

    @GetMapping("/listPage")
    public ResponseDTO<PageResponseDTO<IdSegmentVO>> listPage(IdSegmentListParam param) {
        return ResponseDTOHelper.success(IdSegmentTransfer.INSTANCE.poListToVOList(idSegmentService.listPage(param)));
    }

    @GetMapping("/listBizType")
    public ResponseDTO<List<String>> listBizType() {
        return ResponseDTOHelper.success(idSegmentService.listBizType());
    }
}
