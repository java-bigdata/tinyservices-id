package com.ctw.tinyservices.id.portal.service;

import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.entity.IdToken;
import com.ctw.tinyservices.id.portal.dao.token.IdTokenDAO;
import com.ctw.tinyservices.id.portal.entity.token.param.IdTokenListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TongWei.Chen 2022/4/18 13:16
 *
 * id_token
 **/
@Service
public class IdTokenService {

    @Autowired
    private IdTokenDAO idTokenDAO;

    public void save(IdToken idToken) {
        idTokenDAO.save(idToken);
    }

    public List<String> listBizType() {
        return idTokenDAO.listBizType();
    }

    public PageResponseDTO<IdToken> listPage(IdTokenListParam param) {
        return idTokenDAO.listPage(param);
    }

    public IdToken get(Long id) {
        return idTokenDAO.get(id);
    }

    public void update(IdToken idToken) {
        idTokenDAO.update(idToken);
    }

    public void updateDisabled(Integer id, int isDisabled) {
        idTokenDAO.updateDisabled(id, isDisabled);
    }
}
