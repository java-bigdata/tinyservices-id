package com.ctw.tinyservices.id.portal.entity.token.transfer;

import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.entity.IdToken;
import com.ctw.tinyservices.id.portal.entity.token.param.IdTokenParam;
import com.ctw.tinyservices.id.portal.entity.token.param.IdTokenUpdateParam;
import com.ctw.tinyservices.id.portal.entity.token.vo.IdTokenVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author TongWei.Chen 2022/4/18 17:30
 *
 * id_token mapstruct
 **/
@Mapper
public interface IdTokenTransfer {
    IdTokenTransfer INSTANCE = Mappers.getMapper(IdTokenTransfer.class);

    IdToken updateParamToPO(IdTokenUpdateParam param);

    IdToken paramToPO(IdTokenParam param);

    IdTokenVO poToVO(IdToken idToken);

    PageResponseDTO<IdTokenVO> poListToVOList(PageResponseDTO<IdToken> idTokenList);

}
