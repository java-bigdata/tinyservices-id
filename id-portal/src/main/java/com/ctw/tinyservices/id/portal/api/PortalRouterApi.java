package com.ctw.tinyservices.id.portal.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author xm 2022/04/11
 *
 * 前端路由路径控制器
 */
@RestController
public class PortalRouterApi {

    @GetMapping(value = "/index")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @GetMapping(value = "/html/{suffix}")
    public ModelAndView dataStatus(@PathVariable String suffix) {
        return new ModelAndView("/html/" + suffix + "/" + suffix);
    }

    @GetMapping(value = "/html/{prefix}/{suffix}")
    public ModelAndView dataChildren(@PathVariable String prefix ,@PathVariable String suffix) {
        return new ModelAndView("/html/" + prefix +"/" + suffix + "/" + suffix);
    }

    @GetMapping(value = "/item/{suffix}/{type}")
    public ModelAndView itemTypes(@PathVariable String suffix ,@PathVariable String type) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/html/" + suffix + "/" + type + "/" + type);
        return modelAndView;
    }

    @GetMapping(value = "/item/{suffix}/{type}/{id}")
    public ModelAndView itemTypes(@PathVariable String suffix ,@PathVariable String type ,@PathVariable String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/html/" + suffix + "/" + type + "/" + type);
        modelAndView.addObject("id", id);
        return modelAndView;
    }
}
