package com.ctw.tinyservices.id.portal.entity.segment.param;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author TongWei.Chen 2022/4/22 14:01
 *
 * id_segment
 **/
public class IdSegmentParam {
    /**
     * 业务类型，唯一索引
     */
    @NotBlank(message = "业务类型不能为空")
    private String bizType;

    /**
     * 开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同
     */
    @NotNull(message = "beginId不能为空")
    @Min(value = 1, message = "beginId最小值为1")
    private Long beginId;

    /**
     * 步长
     */
    @NotNull(message = "步长不能为空")
    @Min(value = 1, message = "步长最小值为1")
    private Integer step;

    /**
     * 0:不初始化;1:初始化; server服务启动的时候是否需要初始化，sdk方式写0
     */
    @Min(value = 0, message = "是否需要初始化参数值非法")
    @Max(value = 1, message = "是否需要初始化参数值非法")
    private int isNeedInit;

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public Long getBeginId() {
        return beginId;
    }

    public void setBeginId(Long beginId) {
        this.beginId = beginId;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public int getIsNeedInit() {
        return isNeedInit;
    }

    public void setIsNeedInit(int isNeedInit) {
        this.isNeedInit = isNeedInit;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IdSegmentParam{");
        sb.append("bizType='").append(bizType).append('\'');
        sb.append(", beginId=").append(beginId);
        sb.append(", step=").append(step);
        sb.append(", isNeedInit=").append(isNeedInit);
        sb.append('}');
        return sb.toString();
    }
}
