package com.ctw.tinyservices.id.portal.log;

import java.lang.annotation.*;

/**
 * @author TongWei.Chen  2022/04/21 20:05
 *
 * ApiLog Annotation
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiLog {
}
