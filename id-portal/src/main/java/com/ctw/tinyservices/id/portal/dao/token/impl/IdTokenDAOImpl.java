package com.ctw.tinyservices.id.portal.dao.token.impl;

import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.entity.IdToken;
import com.ctw.tinyservices.id.portal.dao.token.IdTokenDAO;
import com.ctw.tinyservices.id.portal.entity.token.param.IdTokenListParam;
import com.ctw.tinyservices.id.portal.utils.JdbcPageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author TongWei.Chen 2022/4/18 13:15
 *
 * id_token
 **/
@Repository
public class IdTokenDAOImpl implements IdTokenDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private JdbcPageUtils jdbcPageUtils;

    @Transactional
    @Override
    public void save(IdToken idToken) {
        jdbcTemplate.update(
                "INSERT INTO ts_id_token(biz_type, biz_name, token, dept_name, owner_name, owner_email, remark, is_disabled) VALUES(?,?,?,?,?,?,?,?)",
                new Object[]{idToken.getBizType(), idToken.getBizName(), idToken.getToken(), idToken.getDeptName(), idToken.getOwnerName(), idToken.getOwnerEmail(), idToken.getRemark(), idToken.getIsDisabled()});
    }

    @Transactional
    @Override
    public void update(IdToken idToken) {
        jdbcTemplate.update(
                "UPDATE ts_id_token SET biz_name = ?, token = ?, dept_name = ?, owner_name = ?, owner_email = ?, remark = ?, is_disabled = ? WHERE id = ?",
                new Object[]{idToken.getBizName(), idToken.getToken(), idToken.getDeptName(), idToken.getOwnerName(), idToken.getOwnerEmail(), idToken.getRemark(), idToken.getIsDisabled(), idToken.getId()});
    }

    @Override
    public IdToken get(Long id) {
        List<IdToken> list = jdbcTemplate.query("SELECT id, biz_type, biz_name, token, dept_name, owner_name, owner_email, remark, is_disabled, create_time, update_time FROM ts_id_token WHERE id = ?",
                new Object[]{id},
                new IdTokenRowMapper());
        if (CollectionUtils.isEmpty(list)) {
            return new IdToken();
        }
        return list.get(0);
    }

    @Override
    public PageResponseDTO<IdToken> listPage(IdTokenListParam param) {
        StringBuilder sql = new StringBuilder("SELECT id, biz_type, biz_name, token, dept_name, owner_name, owner_email, remark, is_disabled, create_time, update_time FROM ts_id_token WHERE 1 = 1 ");
        StringBuilder countSQL = new StringBuilder("SELECT COUNT(id) FROM ts_id_token WHERE 1 = 1 ");
        Object[] sqlParam = new Object[]{};
        if (! StringUtils.isEmpty(param.getBizType())) {
            sql.append("AND biz_type = '").append(param.getBizType()).append("'");
            countSQL.append("AND biz_type = '").append(param.getBizType()).append("'");
        }
        if (null != param.getIsDisabled()) {
            sql.append("AND is_disabled = ").append(param.getIsDisabled());
            countSQL.append("AND is_disabled = ").append(param.getIsDisabled());
        }

        return jdbcPageUtils.listPage(
                sql.toString(),
                sqlParam,
                new IdTokenRowMapper(),
                countSQL.toString(),
                sqlParam,
                param.getPageNo(),
                param.getPageSize()
                );
    }

    @Override
    public List<String> listBizType() {
        return jdbcTemplate.query(
                "SELECT biz_type FROM ts_id_token",
                new IdTokenOnlyBizTypeRowMapper());
    }

    @Override
    public void updateDisabled(Integer id, int isDisabled) {
        jdbcTemplate.update(
                "UPDATE ts_id_token SET is_disabled = ? WHERE id = ?",
                new Object[]{isDisabled, id});
    }

    public static class IdTokenRowMapper implements RowMapper<IdToken> {
        @Override
        public IdToken mapRow(ResultSet resultSet, int i) throws SQLException {
            IdToken idToken = new IdToken();
            idToken.setId(resultSet.getInt("id"));
            idToken.setBizType(resultSet.getString("biz_type"));
            idToken.setBizName(resultSet.getString("biz_name"));
            idToken.setToken(resultSet.getString("token"));
            idToken.setDeptName(resultSet.getString("dept_name"));
            idToken.setOwnerName(resultSet.getString("owner_name"));
            idToken.setOwnerEmail(resultSet.getString("owner_email"));
            idToken.setRemark(resultSet.getString("remark"));
            idToken.setIsDisabled(resultSet.getInt("is_disabled"));
            idToken.setCreateTime(resultSet.getDate("create_time"));
            idToken.setUpdateTime(resultSet.getDate("update_time"));
            return idToken;
        }
    }

    public static class IdTokenOnlyBizTypeRowMapper implements RowMapper<String> {
        @Override
        public String mapRow(ResultSet resultSet, int i) throws SQLException {
            return resultSet.getString("biz_type");
        }
    }

}
