package com.ctw.tinyservices.id.portal.service;

import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.entity.IdSegment;
import com.ctw.tinyservices.id.portal.dao.segment.IdSegmentDAO;
import com.ctw.tinyservices.id.portal.entity.segment.param.IdSegmentListParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author TongWei.Chen 2022/4/22 13:58
 *
 * id_segment
 **/
@Service
public class IdSegmentService {

    @Autowired
    private IdSegmentDAO idSegmentDAO;

    public void save(IdSegment idSegment) {
        idSegment.setMaxId(idSegment.getBeginId());
        idSegmentDAO.save(idSegment);
    }

    public void delete(Long id) {
        idSegmentDAO.delete(id);
    }

    public IdSegment get(Long id) {
        return idSegmentDAO.get(id);
    }

    public PageResponseDTO<IdSegment> listPage(IdSegmentListParam param) {
        return idSegmentDAO.listPage(param);
    }

    public List<String> listBizType() {
        return idSegmentDAO.listBizType();
    }
}
