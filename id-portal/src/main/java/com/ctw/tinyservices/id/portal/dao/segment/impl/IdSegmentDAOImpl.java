package com.ctw.tinyservices.id.portal.dao.segment.impl;

import com.ctw.tinyservices.id.common.dto.PageResponseDTO;
import com.ctw.tinyservices.id.common.entity.IdSegment;
import com.ctw.tinyservices.id.portal.dao.segment.IdSegmentDAO;
import com.ctw.tinyservices.id.portal.entity.segment.param.IdSegmentListParam;
import com.ctw.tinyservices.id.portal.utils.JdbcPageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author TongWei.Chen 2022/4/22 13:15
 *
 * id_segment
 **/
@Repository
public class IdSegmentDAOImpl implements IdSegmentDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private JdbcPageUtils jdbcPageUtils;

    @Transactional
    @Override
    public void save(IdSegment idSegment) {
        jdbcTemplate.update(
                "INSERT INTO ts_id_segment(biz_type, begin_id, max_id, step, is_need_init) VALUES(?,?,?,?,?)",
                new Object[]{idSegment.getBizType(), idSegment.getBeginId(), idSegment.getMaxId(), idSegment.getStep(), idSegment.getIsNeedInit()});
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update(
                "DELETE FROM ts_id_segment WHERE id = ?", new Object[]{id});
    }

    @Override
    public IdSegment get(Long id) {
        List<IdSegment> list = jdbcTemplate.query("SELECT id, biz_type, begin_id, max_id, step, is_need_init, create_time, update_time FROM ts_id_segment WHERE id = ?",
                new Object[]{id},
                new IdSegmentRowMapper());
        if (CollectionUtils.isEmpty(list)) {
            return new IdSegment();
        }
        return list.get(0);
    }

    @Override
    public PageResponseDTO<IdSegment> listPage(IdSegmentListParam param) {
        StringBuilder sql = new StringBuilder("SELECT id, biz_type, begin_id, max_id, step, is_need_init, create_time, update_time FROM ts_id_segment WHERE 1 = 1 ");
        StringBuilder countSQL = new StringBuilder("SELECT COUNT(id) FROM ts_id_segment WHERE 1 = 1 ");
        Object[] sqlParam = new Object[]{};
        if (! StringUtils.isEmpty(param.getBizType())) {
            sql.append("AND biz_type = '").append(param.getBizType()).append("'");
            countSQL.append("AND biz_type = '").append(param.getBizType()).append("'");
        }

        return jdbcPageUtils.listPage(
                sql.toString(),
                sqlParam,
                new IdSegmentRowMapper(),
                countSQL.toString(),
                sqlParam,
                param.getPageNo(),
                param.getPageSize()
                );
    }

    @Override
    public List<String> listBizType() {
        return jdbcTemplate.query(
                "SELECT biz_type FROM ts_id_segment",
                new IdSegmentOnlyBizTypeRowMapper());
    }

    public static class IdSegmentRowMapper implements RowMapper<IdSegment> {
        @Override
        public IdSegment mapRow(ResultSet resultSet, int i) throws SQLException {
            IdSegment idSegment = new IdSegment();
            idSegment.setId(resultSet.getLong("id"));
            idSegment.setBizType(resultSet.getString("biz_type"));
            idSegment.setBeginId(resultSet.getLong("begin_id"));
            idSegment.setMaxId(resultSet.getLong("max_id"));
            idSegment.setStep(resultSet.getInt("step"));
            idSegment.setIsNeedInit(resultSet.getInt("is_need_init"));
            idSegment.setCreateTime(resultSet.getDate("create_time"));
            idSegment.setUpdateTime(resultSet.getDate("update_time"));
            return idSegment;
        }
    }

    public static class IdSegmentOnlyBizTypeRowMapper implements RowMapper<String> {
        @Override
        public String mapRow(ResultSet resultSet, int i) throws SQLException {
            return resultSet.getString("biz_type");
        }
    }

}
