CREATE DATABASE IF NOT EXISTS ts_id DEFAULT CHARACTER SET = utf8mb4;
Use ts_id;

-- ----------------------------
-- Table structure for ts_id_segment
-- ----------------------------
DROP TABLE IF EXISTS `ts_id_segment`;
CREATE TABLE `ts_id_segment` (
    `id` bigint NOT NULL AUTO_INCREMENT COMMENT '自增主键',
    `biz_type` varchar(63) NOT NULL DEFAULT '' COMMENT '业务类型，唯一',
    `begin_id` bigint NOT NULL DEFAULT '0' COMMENT '开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同',
    `max_id` bigint NOT NULL DEFAULT '0' COMMENT '当前最大id',
    `step` int NOT NULL DEFAULT '1000' COMMENT '步长',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `is_need_init` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:不初始化;1:初始化; server服务启动的时候是否需要初始化，sdk方式写0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uniq_biz_type` (`biz_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='号段信息表';

-- ----------------------------
-- Table structure for ts_id_token
-- ----------------------------
CREATE TABLE `ts_id_token` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
    `biz_type` varchar(63) NOT NULL DEFAULT '' COMMENT '此token可访问的业务类型标识，全局唯一',
    `biz_name` varchar(100) NOT NULL DEFAULT '' COMMENT '应用名',
    `token` varchar(127) NOT NULL DEFAULT '' COMMENT 'token',
    `dept_name` varchar(63) NOT NULL DEFAULT '' COMMENT '部门名称',
    `owner_name` varchar(50) DEFAULT '' COMMENT '负责人：中文名称',
    `owner_email` varchar(63) DEFAULT '' COMMENT '负责人：邮箱前缀',
    `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: deleted, 0: normal',
    `remark` varchar(255) DEFAULT '' COMMENT '备注',
    `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`id`),
    KEY `uniq_biz_type` (`biz_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='应用信息表';

-- ----------------------------
-- Table structure for ts_id_worker_id
-- ----------------------------
CREATE TABLE `ts_id_worker_id` (
   `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
   `ip` varchar(63) NOT NULL DEFAULT '' COMMENT 'ip',
   `port` int(11) NOT NULL DEFAULT -1 COMMENT 'port',
   `ip_port` varchar(127) NOT NULL DEFAULT '' COMMENT 'ip:port',
   `max_timestamp` bigint NOT NULL DEFAULT 0 COMMENT '最后一次心跳时间戳',
   `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
   `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
   PRIMARY KEY (`id`),
   UNIQUE KEY `uniq_ip_port` (`ip`,`port`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='workerId表';