#!/bin/sh

Env=$1
Program_Name=$2
if [ $Env == "dev" ]; then
    echo "use dev config"
    id_server_config_db_driver='com.mysql.cj.jdbc.Driver'
    id_server_config_db_url='jdbc:mysql://localhost:3306/ts_id?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=GMT'
    id_server_config_db_username='root'
    id_server_config_db_password='12345678'
    # The following can be left out and take the druid default value
    id_server_config_db_initial_size=
    id_server_config_db_min_idle=
    id_server_config_db_max_active=
    id_server_config_db_max_wait=

    # next for id-server start
    # Has default value, Can be null
    id_server_snowflake_mode=
    id_server_snowflake_port=
    id_server_snowflake_epoch=
    id_server_snowflake_zkConnectionAddr=
    # next for id-server end

    id_server_log_dir=/data/logs/id-server
    id_portal_log_dir=/data/logs/id-portal
    id_server_port=9996
    id_portal_port=9995
elif [ $Env == "test" ]; then
    echo "use qa config"
    id_server_config_db_driver='com.mysql.cj.jdbc.Driver'
    id_server_config_db_url='jdbc:mysql://localhost:3306/ts_id?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=GMT'
    id_server_config_db_username='root'
    id_server_config_db_password='12345678'
    # The following can be left out and take the druid default value
    id_server_config_db_initial_size=
    id_server_config_db_min_idle=
    id_server_config_db_max_active=
    id_server_config_db_max_wait=

    # next for id-server start
    id_server_snowflake_mode=ZOOKEEPER
    id_server_snowflake_port=6666
    id_server_snowflake_epoch=1648742400000
    id_server_snowflake_zkConnectionAddr=
    # next for id-server end

    id_server_log_dir=/data/logs/id-server
    id_portal_log_dir=/data/logs/id-portal
    id_server_port=9990
    id_portal_port=9991
elif [ $Env == "pre" ]; then
    echo "use pre config"
    id_server_config_db_driver='com.mysql.cj.jdbc.Driver'
    id_server_config_db_url='jdbc:mysql://localhost:3306/ts_id?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=GMT'
    id_server_config_db_username='root'
    id_server_config_db_password='12345678'
    # The following can be left out and take the druid default value
    id_server_config_db_initial_size=
    id_server_config_db_min_idle=
    id_server_config_db_max_active=
    id_server_config_db_max_wait=

    # next for id-server start
    id_server_snowflake_mode=RECYCLABLE_ZOOKEEPER
    id_server_snowflake_port=6666
    id_server_snowflake_epoch=1648742400000
    id_server_snowflake_zkConnectionAddr=localhost:2181,localhost:2182
    # next for id-server end

    id_server_log_dir=/data/logs/id-server
    id_portal_log_dir=/data/logs/id-portal
    id_server_port=9990
    id_portal_port=9991
else
    echo "use online config"
    id_server_config_db_driver='com.mysql.cj.jdbc.Driver'
    id_server_config_db_url='jdbc:mysql://localhost:3306/ts_id?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=GMT'
    id_server_config_db_username='root'
    id_server_config_db_password='12345678'
    # The following can be left out and take the druid default value
    id_server_config_db_initial_size=
    id_server_config_db_min_idle=
    id_server_config_db_max_active=
    id_server_config_db_max_wait=

    # next for id-server start
    id_server_snowflake_mode=MYSQL
    id_server_snowflake_port=6666
    id_server_snowflake_epoch=1648742400000
    id_server_snowflake_zkConnectionAddr=
    # next for id-server end

    id_server_log_dir=/data/logs/id-server
    id_portal_log_dir=/data/logs/id-portal
    id_server_port=9990
    id_portal_port=9991
fi

# go to script directory
cd "${0%/*}" || exit
cd ..

if [ -z "$Program_Name" -o "$Program_Name" == "id-server" ]; then
# moudle id-server
echo "==== starting to build id-server  ===="
mvn clean package -DskipTests -pl id-server -am -Did_server_snowflake_mode=$id_server_snowflake_mode -Did_server_snowflake_port=$id_server_snowflake_port -Did_server_snowflake_epoch=$id_server_snowflake_epoch -Did_server_snowflake_zkConnectionAddr=$id_server_snowflake_zkConnectionAddr -Dserver_port=$id_server_port -Dlog_dir=$id_server_log_dir -Dspring_datasource_driver=$id_server_config_db_driver -Dspring_datasource_url=$id_server_config_db_url -Dspring_datasource_username=$id_server_config_db_username -Dspring_datasource_password=$id_server_config_db_password -Dspring_datasource_initial_size=$id_server_config_db_initial_size -Dspring_datasource_min_idle=$id_server_config_db_min_idle -Dspring_datasource_max_active=$id_server_config_db_max_active -Dspring_datasource_max_wait=$id_server_config_db_max_wait
echo "==== building id-server finished ===="
fi

if [ -z "$Program_Name" -o "$Program_Name" == "id-portal" ]; then
# moudle id-portal
echo "==== starting to build id-portal  ===="
mvn clean package -DskipTests -pl id-portal -am -Dserver_port=$id_portal_port -Dlog_dir=$id_portal_log_dir -Dspring_datasource_driver=$id_server_config_db_driver -Dspring_datasource_url=$id_server_config_db_url -Dspring_datasource_username=$id_server_config_db_username -Dspring_datasource_password=$id_server_config_db_password -Dspring_datasource_initial_size=$id_server_config_db_initial_size -Dspring_datasource_min_idle=$id_server_config_db_min_idle -Dspring_datasource_max_active=$id_server_config_db_max_active -Dspring_datasource_max_wait=$id_server_config_db_max_wait
echo "==== building id-portal finished ===="
fi