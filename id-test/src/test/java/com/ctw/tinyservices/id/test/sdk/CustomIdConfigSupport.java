package com.ctw.tinyservices.id.test.sdk;

import com.ctw.tinyservices.id.sdk.support.AbstractIdConfigSupport;

/**
 * @author TongWei.Chen 2022/3/28 15:52
 **/
public class CustomIdConfigSupport extends AbstractIdConfigSupport {

    @Override
    public String getServerUrl() {
        return "https://gitee.com/tinyservices/tinyservices-id";
    }

    @Override
    public String getBizToken() {
        return null;
    }
}
