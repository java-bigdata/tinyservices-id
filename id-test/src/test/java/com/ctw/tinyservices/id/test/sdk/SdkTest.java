package com.ctw.tinyservices.id.test.sdk;

import com.ctw.tinyservices.id.sdk.AbstractIdSdk;
import com.ctw.tinyservices.id.sdk.enums.IdSdkTypeEnum;
import com.ctw.tinyservices.id.sdk.factory.IdSdkFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author TongWei.Chen 2022/3/25 14:54
 **/
public class SdkTest {

    private static final Long VALUE = 100_0000L;

    @Test
    public void testSegment() {
        AbstractIdSdk idSdk = IdSdkFactory.getInstance().getIdSdk(IdSdkTypeEnum.SEGMENT, "test_sdk.properties").registerConfigSupport();
        doTest(idSdk);
    }

    @Test
    public void testSnowflake() {
        AbstractIdSdk idSdk = IdSdkFactory.getInstance().getIdSdk(IdSdkTypeEnum.SNOWFLAKE, "test_sdk.properties").registerConfigSupport(/*new CustomIdConfigSupport()*/);
        doTest(idSdk);
    }

    private void doTest(AbstractIdSdk idSdk) {
        long start = System.currentTimeMillis();
        Set<Long> idsSet = Sets.newHashSet();
        List<Long> nullList = Lists.newArrayList();
        for (int i = 0; i < VALUE; i++) {
            Long ctw = idSdk.getId("sdk1");
            if (null == ctw) {
                nullList.add(1L);
                continue;
            }
            idsSet.add(ctw);
        }
        System.out.println(System.currentTimeMillis() - start);
        assertTrue(nullList.size() == 0);
        assertTrue(idsSet.size() == VALUE);
    }
}
