package com.ctw.tinyservices.id.sdk.support;

import com.ctw.tinyservices.id.common.utils.PropertiesUtils;
import com.ctw.tinyservices.id.sdk.util.NumberUtils;
import com.google.common.base.Strings;

import java.util.Properties;

/**
 * @author TongWei.Chen 2022/3/28 13:59
 *
 * 默认id配置类
 **/
public class DefaultIdConfigSupport extends AbstractIdConfigSupport {

    private Properties properties;

    public DefaultIdConfigSupport(String location) {
        properties = PropertiesUtils.load(Strings.isNullOrEmpty(location) ? DEFAULT_PROP : location);
    }

    @Override
    public String getServerUrl() {
        return PropertiesUtils.getPropertiesEnvPriority(properties, SERVER_URL_KEY);
    }

    @Override
    public int getReadTimeout() {
        return NumberUtils.toInt(PropertiesUtils.getPropertiesEnvPriority(properties, READ_TIMEOUT_KEY), DEFAULT_TIME_OUT);
    }

    @Override
    public int getConnectTimeout() {
        return NumberUtils.toInt(PropertiesUtils.getPropertiesEnvPriority(properties, CONNECT_TIMEOUT_KEY), DEFAULT_TIME_OUT);
    }

    @Override
    public String getBizToken() {
        return PropertiesUtils.getPropertiesEnvPriority(properties, BIZ_TOKEN_KEY);
    }

    @Override
    public long getEpoch() {
        String epoch = PropertiesUtils.getPropertiesEnvPriority(properties, SNOWFLAKE_EPOCH);
        return Strings.isNullOrEmpty(epoch) ? super.getEpoch() : Long.parseLong(epoch);
    }
}
