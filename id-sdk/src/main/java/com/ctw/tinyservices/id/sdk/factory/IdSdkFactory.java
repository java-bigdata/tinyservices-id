package com.ctw.tinyservices.id.sdk.factory;

import com.ctw.tinyservices.id.sdk.AbstractIdSdk;
import com.ctw.tinyservices.id.sdk.enums.IdSdkTypeEnum;
import com.ctw.tinyservices.id.sdk.segment.IdSegmentSdk;
import com.ctw.tinyservices.id.sdk.snowflake.IdSnowflakeSdk;
import com.google.common.base.Strings;

/**
 * @author TongWei.Chen 2022/3/25 14:04
 *
 * Id SDK方式工厂
 **/
public class IdSdkFactory {

    private IdSdkFactory() {}

    private static class SingletonHolder {
        private static final IdSdkFactory INSTANCE = new IdSdkFactory();
    }

    public static final IdSdkFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public AbstractIdSdk getIdSdk(IdSdkTypeEnum sdkTypeEnum, String location) {
        if (! Strings.isNullOrEmpty(location) && ! location.endsWith("properties")) throw new IllegalArgumentException("配置文件类型必须为properties, 当前文件:" + location);
        AbstractIdSdk instance = null;
        switch (sdkTypeEnum) {
            case SEGMENT:
                instance = IdSegmentSdk.getInstance(location);
                break;
            case SNOWFLAKE:
                instance = IdSnowflakeSdk.getInstance(location);
                break;
            default:
                break;
        }
        return instance;
    }

    public AbstractIdSdk getIdSdk(IdSdkTypeEnum sdkTypeEnum) {
        return getIdSdk(sdkTypeEnum, null);
    }

}
