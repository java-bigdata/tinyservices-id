package com.ctw.tinyservices.id.sdk.config;

import java.util.List;

/**
 * @author TongWei.Chen 2022/3/22 19:53
 *
 * IdSdk配置类
 **/
public class IdSdkConfig {

    private List<String> serverList;
    private Integer readTimeout;
    private Integer connectTimeout;
    private String bizToken;

    private IdSdkConfig() {}

    private volatile static IdSdkConfig singleton;

    public static IdSdkConfig getInstance() {
        if (singleton == null) {
            synchronized (IdSdkConfig.class) {
                if (singleton == null) {
                    singleton = new IdSdkConfig();
                }
            }
        }
        return singleton;
    }

    public List<String> getServerList() {
        return serverList;
    }

    public void setServerList(List<String> serverList) {
        this.serverList = serverList;
    }

    public Integer getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Integer readTimeout) {
        this.readTimeout = readTimeout;
    }

    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Integer connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public String getBizToken() {
        return bizToken;
    }

    public void setBizToken(String bizToken) {
        this.bizToken = bizToken;
    }
}
