package com.ctw.tinyservices.id.sdk.enums;

/**
 * @author TongWei.Chen 2022/3/25 10:53
 *
 * MethodBaseEnum
 **/
public interface MethodBaseEnum {

    String getMethod();

}
