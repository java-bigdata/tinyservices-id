package com.ctw.tinyservices.id.sdk.util;

/**
 * @author TongWei.Chen 2022/03/08 12:16
 *
 * 数字工具类
 **/
public class NumberUtils {

    public static int toInt(String str) {
        return toInt(str, 0);
    }

    public static int toInt(String str, int defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return defaultValue;
        }
    }
}
