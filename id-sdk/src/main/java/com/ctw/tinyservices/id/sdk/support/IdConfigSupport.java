package com.ctw.tinyservices.id.sdk.support;

/**
 * @author TongWei.Chen 2022/3/28 13:59
 *
 * id配置接口
 **/
public interface IdConfigSupport {
    String getBizToken();
    String getServerUrl();
    int getReadTimeout();
    int getConnectTimeout();

    /**
     * snowflake
     */
    long getEpoch();
}
