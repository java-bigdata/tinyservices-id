package com.ctw.tinyservices.id.sdk.enums;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author TongWei.Chen 2022/3/25 10:53
 *
 * Segment api 接口名
 **/
public enum HttpMethodEnum implements MethodBaseEnum {

    GET_SEGMENT_BUFFER("segment/getSegmentBuffer"),
    UPDATE_MAXID_AND_GET("segment/updateMaxIdAndGet"),
    UPDATE_MAXID_BY_STEP_AND_GET("segment/updateMaxIdByStepAndGet"),

    GET_SNOWFLAKE_WORKERID("snowflake/getWorkerId"),
    SNOWFLAKE_AUTH("snowflake/auth"),
    ;

    private String method;

    HttpMethodEnum(String method) {
        this.method = method;
    }

    public static final String getSegmentBufferParam(String bizType) {
        return new StringBuilder("/").append(bizType).toString();
    }

    public static final Map<String, String> getUpdateMaxIdAndGetParam(String bizType) {
        Map<String, String> paramMap = Maps.newHashMap();
        paramMap.put("bizType", bizType);
        return paramMap;
    }

    public static final Map<String, String> getUpdateMaxIdByStepAndGetParam(String bizType, int step) {
        Map<String, String> paramMap = Maps.newHashMap();
        paramMap.put("bizType", bizType);
        paramMap.put("step", String.valueOf(step));
        return paramMap;
    }

    public static final String getSnowflakeAuthParam(String bizType) {
        return new StringBuilder("/").append(bizType).toString();
    }

    @Override
    public String getMethod() {
        return this.method;
    }
}
