package com.ctw.tinyservices.id.sdk.support;

/**
 * @author TongWei.Chen 2022/3/28 13:59
 *
 * id配置，支持自定义扩展
 **/
public abstract class AbstractIdConfigSupport implements IdConfigSupport {
    protected static final String DEFAULT_PROP = "tinyservices_id_sdk.properties";

    protected static final String BIZ_TOKEN_KEY = "tinyservices.id.token";
    protected static final String SERVER_URL_KEY = "tinyservices.id.server";
    protected static final String READ_TIMEOUT_KEY = "tinyservices.id.readTimeout";
    protected static final String CONNECT_TIMEOUT_KEY = "tinyservices.id.connectTimeout";
    protected static final int DEFAULT_TIME_OUT = 5000;

    /**
     * snowflake
     */
    protected static final String SNOWFLAKE_EPOCH = "tinyservices.id.snowflake.epoch";

    @Override
    public abstract String getServerUrl();

    @Override
    public int getReadTimeout() {
        return DEFAULT_TIME_OUT;
    }

    @Override
    public int getConnectTimeout() {
        return DEFAULT_TIME_OUT;
    }

    @Override
    public long getEpoch() {
        // BeiJing: 2022-04-01 00:00:00
        return 1648742400000L;
    }
}
