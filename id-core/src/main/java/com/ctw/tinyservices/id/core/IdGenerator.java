package com.ctw.tinyservices.id.core;

import com.ctw.tinyservices.id.common.lifecycle.IdLifecycle;

/**
 * @author TongWei.Chen 2022/3/4 13:39
 *
 * IdGenerator
 **/
public interface IdGenerator extends IdLifecycle {

    /**
     * get next id
     *
     * @return id
     */
    Long getId(String bizType);

}
