package com.ctw.tinyservices.id.core.segment.service;

import com.ctw.tinyservices.id.common.entity.IdSegment;

import java.util.List;

public interface IdSegmentService {

    /**
     * 获取全部号段
     *
     * @return 全部号段
     */
    List<IdSegment> list();

    /**
     * 更新最大id且返回更新后的号段
     *
     * @param bizType 业务类型
     * @return 更新后的号段
     */
    IdSegment updateMaxIdAndGet(String bizType);

    /**
     * 更新最大id且返回更新后的号段
     *
     * @param bizType 业务类型
     * @param nextStep 下一个步长
     * @return 更新后的号段
     */
    IdSegment updateMaxIdByStepAndGet(String bizType, int nextStep);
}
