package com.ctw.tinyservices.id.core.snowflake.exception;

import com.ctw.tinyservices.id.common.exception.TinyServicesIdException;

public class OverMaxTimestampException extends TinyServicesIdException {

    public OverMaxTimestampException(String msg){
        super(msg);
    }
}
