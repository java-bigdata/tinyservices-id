package com.ctw.tinyservices.id.core.snowflake.exception;

import com.ctw.tinyservices.id.common.exception.TinyServicesIdException;

public class ClockBackException extends TinyServicesIdException {

    public ClockBackException(String msg){
        super(msg);
    }
}
