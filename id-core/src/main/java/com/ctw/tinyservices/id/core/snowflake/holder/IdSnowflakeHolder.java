package com.ctw.tinyservices.id.core.snowflake.holder;

public interface IdSnowflakeHolder {
    int getWorkerId();
}
