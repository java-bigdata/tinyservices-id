package com.ctw.tinyservices.id.core.snowflake.exception;

import com.ctw.tinyservices.id.common.exception.TinyServicesIdException;

/**
 * @author TongWei.Chen 2022/4/14 14:33
 **/
public class ZookeeperConnectFailException extends TinyServicesIdException {
    public ZookeeperConnectFailException(String msg) {
        super(msg);
    }
}
