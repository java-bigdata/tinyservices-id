package com.ctw.tinyservices.id.core.snowflake.enums;

public enum IdSnowflakeHolderTypeEnum {
    LOCAL,
    MYSQL,
    ZOOKEEPER,
    RECYCLABLE_ZOOKEEPER
    ;
}