package com.ctw.tinyservices.id.core.snowflake.service;

import com.ctw.tinyservices.id.common.entity.IdWorkerId;

public interface IdWorkerIdService {

    IdWorkerId getOrInsert(String ip, int port, String ipPort);

    int updateMaxTimestamp(int workerId, long currentTime);
}
