package com.ctw.tinyservices.id.core;

import com.ctw.tinyservices.id.common.lifecycle.AbstractIdLifecycle;

/**
 * @author TongWei.Chen 2022/3/4 13:39
 *
 * AbstractIdGenerator
 **/
public abstract class AbstractIdGenerator extends AbstractIdLifecycle implements IdGenerator {

    @Override
    public String name() {
        return "IdGenerator";
    }

}
