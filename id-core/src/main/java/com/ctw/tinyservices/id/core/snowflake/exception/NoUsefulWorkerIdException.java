package com.ctw.tinyservices.id.core.snowflake.exception;

import com.ctw.tinyservices.id.common.exception.TinyServicesIdException;

/**
 * @author TongWei.Chen 2022/4/8 19:37
 *
 * CheckLastTimeException
 **/
public class NoUsefulWorkerIdException extends TinyServicesIdException {

    public NoUsefulWorkerIdException(String msg){
        super(msg);
    }
}
