package com.ctw.tinyservices.id.core.snowflake.exception;

import com.ctw.tinyservices.id.common.exception.TinyServicesIdException;

/**
 * @author TongWei.Chen 2022/4/13 13:37
 *
 * LocalWorkIdNotFoundException
 **/
public class LocalWorkIdNotFoundException extends TinyServicesIdException {

    public LocalWorkIdNotFoundException(String msg){
        super(msg);
    }
}
