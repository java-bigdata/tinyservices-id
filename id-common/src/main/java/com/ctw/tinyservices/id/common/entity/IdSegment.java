package com.ctw.tinyservices.id.common.entity;

import java.util.Date;

/**
 * @author TongWei.Chen 2022/3/3 13:17
 *
 * Id号段表
 **/
public class IdSegment {
    /**
     * 主键id
     */
    private Long id;

    /**
     * 业务类型，唯一索引
     */
    private String bizType;

    /**
     * 开始id，仅记录初始值，无其他含义。初始化时begin_id和max_id应相同
     */
    private Long beginId;

    /**
     * 当前最大id
     */
    private Long maxId;

    /**
     * 步长
     */
    private Integer step;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 0:不初始化;1:初始化; server服务启动的时候是否需要初始化，sdk方式写0
     */
    private int isNeedInit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public Long getBeginId() {
        return beginId;
    }

    public void setBeginId(Long beginId) {
        this.beginId = beginId;
    }

    public Long getMaxId() {
        return maxId;
    }

    public void setMaxId(Long maxId) {
        this.maxId = maxId;
    }

    public Integer getStep() {
        return step;
    }

    public void setStep(Integer step) {
        this.step = step;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getIsNeedInit() {
        return isNeedInit;
    }

    public void setIsNeedInit(int isNeedInit) {
        this.isNeedInit = isNeedInit;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IdSegment{");
        sb.append("id=").append(id);
        sb.append(", bizType='").append(bizType).append('\'');
        sb.append(", beginId=").append(beginId);
        sb.append(", maxId=").append(maxId);
        sb.append(", step=").append(step);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", isNeedInit=").append(isNeedInit);
        sb.append('}');
        return sb.toString();
    }
}
