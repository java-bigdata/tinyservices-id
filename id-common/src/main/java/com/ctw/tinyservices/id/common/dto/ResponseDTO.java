package com.ctw.tinyservices.id.common.dto;

/**
 * @author TongWei.Chen 2022/4/1 15:28
 *
 * api接口返回dto
 **/
public class ResponseDTO<T> {
    /**
     * 状态码
     */
    private int code;

    /**
     * 提示消息
     */
    private String msg;

    /**
     * 返回内容
     */
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
