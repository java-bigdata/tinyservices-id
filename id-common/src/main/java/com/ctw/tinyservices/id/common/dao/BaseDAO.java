package com.ctw.tinyservices.id.common.dao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TongWei.Chen 2022/3/3 13:17
 *
 * BaseDAO
 **/
public interface BaseDAO<T> {

    /**
     * 查询全部信息
     *
     * @return List
     */
    default List<T> list() {
        return new ArrayList<>();
    }

    /**
     * 插入
     *
     * @param t 数据体
     */
    default void save(T t) {}

    /**
     * 更新
     *
     * @param t 数据体
     */
    default void update(T t) {}

    /**
     * 删除
     *
     * @param id 主键id
     */
    default void delete(Long id) {}

    /**
     * 根据主键查
     *
     * @param id 主键id
     * @return 数据体
     */
    default T get(Long id) {
        return null;
    }
}
