package com.ctw.tinyservices.id.common.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.ctw.tinyservices.id.common.exception.DataSourceInitException;
import com.ctw.tinyservices.id.common.utils.LogUtils;
import com.ctw.tinyservices.id.common.utils.PropertiesUtils;

import java.sql.SQLException;
import java.util.Properties;

/**
 * @author TongWei.Chen 2022/4/2 17:11
 *
 * 数据源配置
 **/
public class DataSourceConfig {
    private static final String JDBC_PROPERTIES_FILENAME = "application-jdbc.properties";

    private DruidDataSource druidDataSource = new DruidDataSource();

    private DataSourceConfig() {
        init();
    }
    private static class SingletonHolder {
        private static final DataSourceConfig INSTANCE = new DataSourceConfig();
    }
    public static final DataSourceConfig getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void init() {
        LogUtils.info(DataSourceConfig.class, "DataSourceConfig init begin...");
        try {
            // Config dataSource
            Properties properties = PropertiesUtils.load(JDBC_PROPERTIES_FILENAME);
            druidDataSource.setDriverClassName(PropertiesUtils.getPropertiesEnvPriority(properties, "spring.datasource.driver"));
            druidDataSource.setUrl(PropertiesUtils.getPropertiesEnvPriority(properties, "spring.datasource.url"));
            druidDataSource.setUsername(PropertiesUtils.getPropertiesEnvPriority(properties, "spring.datasource.username"));
            druidDataSource.setPassword(PropertiesUtils.getPropertiesEnvPriority(properties, "spring.datasource.password"));
            String initialSize = PropertiesUtils.getPropertiesEnvPriority(properties, "spring.datasource.initial-size");
            if (null != initialSize && initialSize.length() != 0) {
                druidDataSource.setInitialSize(Integer.parseInt(initialSize));
            }
            String minIdle = PropertiesUtils.getPropertiesEnvPriority(properties, "spring.datasource.min-idle");
            if (null != minIdle && minIdle.length() != 0) {
                druidDataSource.setMinIdle(Integer.parseInt(minIdle));
            }
            String maxActive = PropertiesUtils.getPropertiesEnvPriority(properties, "spring.datasource.max-active");
            if (null != maxActive && maxActive.length() != 0) {
                druidDataSource.setMaxActive(Integer.parseInt(maxActive));
            }
            String maxWait = PropertiesUtils.getPropertiesEnvPriority(properties, "spring.datasource.max-wait");
            if (null != maxWait && maxWait.length() != 0) {
                druidDataSource.setMaxWait(Integer.parseInt(maxWait));
            }
            druidDataSource.init();
        } catch (SQLException e) {
            LogUtils.error(DataSourceConfig.class, "DataSourceConfig init error ");
            throw new DataSourceInitException();
        }
        LogUtils.info(DataSourceConfig.class, "DataSourceConfig init end...");
    }

    public DruidDataSource getDruidDataSource() {
        return druidDataSource;
    }
}
