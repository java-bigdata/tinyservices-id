package com.ctw.tinyservices.id.common.exception;

/**
 * @author TongWei.Chen  2022/4/8 13:54
 *
 * tiny-services IdException
 **/
public class TinyServicesIdException extends RuntimeException {

    public TinyServicesIdException(String msg){
        super(msg);
    }
}
