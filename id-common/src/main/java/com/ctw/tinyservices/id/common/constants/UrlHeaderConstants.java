package com.ctw.tinyservices.id.common.constants;

/**
 * @author TongWei.Chen 2022/3/3 13:17
 *
 * 常量 http header的key
 **/
public interface UrlHeaderConstants {
    /**
     * 获取id时需要传递的key，放到header里。
     */
    String ID_TOKEN = "tinyservice-id-token";
}
