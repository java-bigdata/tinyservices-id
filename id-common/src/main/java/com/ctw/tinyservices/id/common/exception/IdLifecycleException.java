package com.ctw.tinyservices.id.common.exception;

/**
 * @author TongWei.Chen 2022/4/8 19:59
 *
 * Id 生命周期异常
 **/
public class IdLifecycleException extends TinyServicesIdException {

    public IdLifecycleException(String msg) {
        super(msg);
    }
}
