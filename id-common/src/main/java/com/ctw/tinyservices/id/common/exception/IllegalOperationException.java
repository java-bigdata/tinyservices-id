package com.ctw.tinyservices.id.common.exception;

/**
 * @author TongWei.Chen 2022/4/18 19:54
 *
 * 非法操作
 **/
public class IllegalOperationException extends TinyServicesIdException {

    public IllegalOperationException(String msg){
        super(msg);
    }
}
