package com.ctw.tinyservices.id.common.dto.helper;

import com.ctw.tinyservices.id.common.dto.ResponseDTO;
import com.ctw.tinyservices.id.common.enums.code.ErrorCodeEnum;
import com.ctw.tinyservices.id.common.enums.code.IBasicCode;

/**
 * @author TongWei.Chen 2022/4/1 16:00
 **/
public class ResponseDTOHelper {

    private ResponseDTOHelper() {}

    /**
     * 请求成功
     *
     * @return ResponseDTO
     */
    public static final <T> ResponseDTO<T> success() {
        return fail(ErrorCodeEnum.SUCCESS, null);
    }

    /**
     * 请求成功，自定义data
     *
     * @param data: 数据
     * @return ResponseDT
     */
    public static final <T> ResponseDTO <T> success(final T data) {
        return success(ErrorCodeEnum.SUCCESS, data);
    }

    /**
     * 请求成功，自定义状态码
     *
     * @param baseEnum：返回给客户端的状态码
     * @param data：返回给客户端的结果
     * @return ResponseDTO
     */
    public static final <T> ResponseDTO <T> success(final IBasicCode baseEnum, final T data) {
        return rtnBaseResp(baseEnum, data);
    }

    /**
     * 请求失败，系统异常
     * @return ResponseDTO
     */
    public static final <T> ResponseDTO <T> fail() {
        return fail(ErrorCodeEnum.FAIL, null);
    }

    /**
     * 请求失败，系统异常
     * @return ResponseDTO
     */
    public static final <T> ResponseDTO <T> fail(T data) {
        return fail(ErrorCodeEnum.FAIL, data);
    }

    /**
     * 请求失败，自定义错误码信息，但返回data为null
     *
     * @param baseEnum：返回给客户端的错误码
     * @return ResponseDTO
     */
    public static final <T> ResponseDTO <T> fail(final IBasicCode baseEnum) {
        return fail(baseEnum, null);
    }

    /**
     * 请求失败，自定义错误码信息
     *
     * @param baseEnum：返回给客户端的错误码
     * @param data：返回给客户端的结果
     * @return ResponseDTO
     */
    public static final <T> ResponseDTO <T> fail(final IBasicCode baseEnum, final T data) {
        return rtnBaseResp(baseEnum, data);
    }

    private static final <T> ResponseDTO <T> rtnBaseResp(final IBasicCode baseEnum, final T data) {
        ResponseDTO response = new ResponseDTO();
        response.setCode(baseEnum.getCode());
        response.setMsg(baseEnum.getMsg());
        response.setData(data);
        return response;
    }
}
