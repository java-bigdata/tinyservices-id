package com.ctw.tinyservices.id.common.enums.code;

/**
 * @author TongWei.Chen 2022/3/7 19:40
 *
 * id生成器生命周期状态
 **/
public enum ErrorCodeEnum implements IBasicCode {
    SUCCESS(0, "Processed successfully"),
    FAIL(-1, "System exception"),
    PARAM_ERROR(-2, "System exception"),
    BATCH_ERROR(-3, "Some requests fail, but others succeed"),
    AUTH_ERROR(-4, "no auth access"),
    DUPLICATE_KEY_ERROR(-5, "unique key conflict")
    ;

    /**
     * 状态码
     */
    private int code;
    /**
     * 消息
     */
    private String msg;

    ErrorCodeEnum(int value, String msg) {
        this.code = value;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
