package com.ctw.tinyservices.id.common.exception;

/**
 * @author TongWei.Chen 2022/3/25 17:01
 *
 * HttpException
 **/
public class HttpException extends TinyServicesIdException {

    private static final String MSG_FORMAT = "http exception, or perhaps the service did not return any data, Maybe not authorized, please check the token configuration, url：%s, param：%s";

    public HttpException(String url, String param) {
        super(String.format(MSG_FORMAT, url, param));
    }
}
