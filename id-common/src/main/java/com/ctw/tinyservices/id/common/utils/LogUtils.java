package com.ctw.tinyservices.id.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author TongWei.Chen 2022/4/01 22:43
 *
 * Log 工具类
 **/
public final class LogUtils {

    private LogUtils() {}

    private static final Map<Class, Logger> LOGGER_MAP = new ConcurrentHashMap<>();

    public static void debug (Class clazz, String msg, Object ... args) {
        cacheLogger(clazz);
        Logger logger = LOGGER_MAP.get(clazz);
        if (logger.isDebugEnabled()) {
            logger.debug(msg, args);
        }
    }

    public static void info (Class clazz, String msg, Object ... args) {
        cacheLogger(clazz);
        Logger logger = LOGGER_MAP.get(clazz);
        if (logger.isInfoEnabled()) {
            logger.info(msg, args);
        }
    }

    public static void warn (Class clazz, String msg, Object ... args) {
        cacheLogger(clazz);
        Logger logger = LOGGER_MAP.get(clazz);
        if (logger.isWarnEnabled()) {
            logger.warn(msg, args);
        }
    }

    public static void error (Class clazz, String msg, Object ... args) {
        cacheLogger(clazz);
        Logger logger = LOGGER_MAP.get(clazz);
        if (logger.isErrorEnabled()) {
            logger.error(msg, args);
        }
    }

    private static void cacheLogger(Class clazz) {
        if (!LOGGER_MAP.containsKey(clazz)) {
            LOGGER_MAP.put(clazz, LoggerFactory.getLogger(clazz));
        }
    }

}
