package com.ctw.tinyservices.id.common.exception;

/**
 * @author TongWei.Chen 2022/3/7 19:59
 *
 * DataSource初始化异常
 **/
public class DataSourceInitException extends TinyServicesIdException {

    public DataSourceInitException() {
        this("DruidDataSource init error");
    }

    public DataSourceInitException(String msg) {
        super(msg);
    }
}
