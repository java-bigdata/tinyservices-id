package com.ctw.tinyservices.id.common.enums.code;

/**
 * @author TongWei.Chen 2022/4/1 16:02
 *
 * 状态码父接口
 **/
public interface IBasicCode {
    /**
     * 获取状态码
     * @return
     */
    int getCode();

    /**
     * 获取消息
     * @return
     */
    String getMsg();
}
