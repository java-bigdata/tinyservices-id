package com.ctw.tinyservices.id.common.lifecycle;

import com.ctw.tinyservices.id.common.enums.state.IdLifecycleState;

/**
 * @author TongWei.Chen 2022/3/7 19:40
 *
 * id生成器生命周期接口
 **/
public interface IdLifecycle {

    /**
     * 获取当前组件的状态
     *
     * @return： 当前组件的状态
     */
    IdLifecycleState getState();

    /**
     * 设置当前组件的状态
     */
    IdLifecycleState setState(IdLifecycleState state);

    /**
     * 初始化
     */
    void init();

    /**
     * 启动
     */
    void start();

    /**
     * 停止
     */
    void stop();

    /**
     * 组件名
     *
     * @return 组件名
     */
    String name();
}
