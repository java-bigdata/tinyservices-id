package com.ctw.tinyservices.id.common.exception;

/**
 * @author TongWei.Chen 2022/3/7 19:59
 *
 * Id服务状态异常
 **/
public class IdStateException extends TinyServicesIdException {

    public IdStateException(String msg) {
        super(msg);
    }
}
