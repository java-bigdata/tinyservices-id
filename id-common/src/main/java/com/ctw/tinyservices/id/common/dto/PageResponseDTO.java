package com.ctw.tinyservices.id.common.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TongWei.Chen 2022/3/3 13:17
 *
 * 分页查询通用DTO
 **/
public class PageResponseDTO<T> {
    /**
     * 总数
     */
    private Integer totalSize;

    /**
     * 总页数
     */
    private Integer totalPage;

    /**
     * 每页数量
     */
    private Integer pageSize;

    /**
     * 当前页数
     */
    private Integer pageNo;

    /**
     * 数据
     */
    private List<T> data;

    public PageResponseDTO() {
        this.data = new ArrayList<>();
    }

    public PageResponseDTO(Integer totalSize, Integer totalPage, Integer pageSize, Integer pageNo, List<T> data) {
        this.totalSize = totalSize;
        this.totalPage = totalPage;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
        this.data = data;
    }

    public Integer getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Integer totalSize) {
        this.totalSize = totalSize;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
