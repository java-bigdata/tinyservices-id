package com.ctw.tinyservices.id.common.enums.state;

/**
 * @author TongWei.Chen 2022/3/7 19:40
 *
 * id生成器生命周期状态
 **/
public enum IdLifecycleState {
    DEFAULT("默认状态", 0),
    INIT("初始化状态", 1),
    START("启动状态", 2),
    STOP("停止状态", 3);

    IdLifecycleState(String msg, int state) {
        this.msg = msg;
        this.state = state;
    }

    private String msg;
    private int state;

    public String msg() {
        return msg;
    }

    public int state() {
        return state;
    }
}
