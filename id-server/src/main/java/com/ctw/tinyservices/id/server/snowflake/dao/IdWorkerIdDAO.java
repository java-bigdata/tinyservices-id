package com.ctw.tinyservices.id.server.snowflake.dao;

import com.ctw.tinyservices.id.common.dao.BaseDAO;
import com.ctw.tinyservices.id.common.entity.IdWorkerId;

/**
 * @author TongWei.Chen 2022/4/8 16:53
 **/
public interface IdWorkerIdDAO extends BaseDAO<IdWorkerId> {

    IdWorkerId getOrInsert(String ip, int port, String ipPort);

    int updateMaxTimestamp(int workerId, long currentTime);
}
