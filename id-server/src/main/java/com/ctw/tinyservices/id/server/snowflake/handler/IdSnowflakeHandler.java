package com.ctw.tinyservices.id.server.snowflake.handler;

import com.ctw.tinyservices.id.common.utils.LogUtils;
import com.ctw.tinyservices.id.core.snowflake.generator.IdSnowflakeGenerator;
import com.ctw.tinyservices.id.server.handler.AbstractIdHandler;
import com.ctw.tinyservices.id.server.snowflake.config.IdSnowflakeConfig;
import com.ctw.tinyservices.id.server.snowflake.factory.IdSnowflakeHolderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author TongWei.Chen 2022/4/8 16:11
 *
 * 雪花算法Handler
 **/
@Component
public class IdSnowflakeHandler extends AbstractIdHandler {

    @Autowired
    private IdSnowflakeConfig idSnowflakeConfig;

    @Override
    protected void doInit() {
        LogUtils.info(IdSnowflakeHandler.class, "IdSnowflakeHandler doInit data begin...");
        idGenerator = new IdSnowflakeGenerator(IdSnowflakeHolderFactory.getInstance().getIdSnowflakeHolder(idSnowflakeConfig), idSnowflakeConfig.getEpoch());
        idGenerator.init();
        LogUtils.info(IdSnowflakeHandler.class, "IdSnowflakeHandler doInit data end...");
    }

    @Override
    protected void doStart() {
        LogUtils.info(IdSnowflakeHandler.class, "IdSnowflakeHandler doStart data begin...");
        idGenerator.start();
        LogUtils.info(IdSnowflakeHandler.class, "IdSnowflakeHandler doStart data end...");
    }

    @Override
    protected void doStop() {
        LogUtils.info(IdSnowflakeHandler.class, "IdSnowflakeHandler doStop data begin...");
        idGenerator.stop();
        LogUtils.info(IdSnowflakeHandler.class, "IdSnowflakeHandler doStop data end...");
    }
}
