package com.ctw.tinyservices.id.server.snowflake.service;

import com.ctw.tinyservices.id.common.entity.IdWorkerId;
import com.ctw.tinyservices.id.core.snowflake.service.IdWorkerIdService;
import com.ctw.tinyservices.id.server.snowflake.dao.IdWorkerIdDAO;

public class IdWorkerIdServiceImpl implements IdWorkerIdService {

    private IdWorkerIdDAO idWorkerIdDAO;

    public IdWorkerIdServiceImpl(IdWorkerIdDAO idWorkerIdDAO) {
        this.idWorkerIdDAO = idWorkerIdDAO;
    }

    @Override
    public IdWorkerId getOrInsert(String ip, int port, String ipPort) {
        return idWorkerIdDAO.getOrInsert(ip, port, ipPort);
    }

    @Override
    public int updateMaxTimestamp(int workerId, long currentTime) {
        return idWorkerIdDAO.updateMaxTimestamp(workerId, currentTime);
    }
}
