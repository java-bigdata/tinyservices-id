package com.ctw.tinyservices.id.server.api;

import com.ctw.tinyservices.id.common.dto.ResponseDTO;
import com.ctw.tinyservices.id.common.dto.helper.ResponseDTOHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author TongWei.Chen 2022/4/14 19:27
 *
 * 健康检查 api
 **/
@RestController
@RequestMapping("/health/check")
public class HealthCheckApi extends AbstractIdApi {

    @GetMapping("")
    public ResponseDTO<Long> healthCheck() {
        return ResponseDTOHelper.success(666L);
    }
}
