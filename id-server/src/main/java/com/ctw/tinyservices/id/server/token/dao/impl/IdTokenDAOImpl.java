package com.ctw.tinyservices.id.server.token.dao.impl;

import com.ctw.tinyservices.id.common.entity.IdToken;
import com.ctw.tinyservices.id.server.token.dao.IdTokenDAO;
import com.google.common.base.Strings;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author TongWei.Chen 2022/3/31 19:08
 *
 * IdTokenDAOImpl
 **/
public class IdTokenDAOImpl implements IdTokenDAO {

    private JdbcTemplate jdbcTemplate;

    public IdTokenDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<IdToken> listEnabled() {
        String sql = "SELECT biz_type, token FROM ts_id_token WHERE is_disabled = 0";
        return jdbcTemplate.query(sql, new IdTokenRowMapper());
    }

    public static class IdTokenRowMapper implements RowMapper<IdToken> {
        @Override
        public IdToken mapRow(ResultSet resultSet, int i) throws SQLException {
            IdToken idToken = new IdToken();
            String bizType = resultSet.getString("biz_type");
            if (! Strings.isNullOrEmpty(bizType)) {
                idToken.setBizType(bizType);
            }
            String token = resultSet.getString("token");
            if (! Strings.isNullOrEmpty(token)) {
                idToken.setToken(token);
            }
            return idToken;
        }
    }
}
