package com.ctw.tinyservices.id.server.token.dao;

import com.ctw.tinyservices.id.common.dao.BaseDAO;
import com.ctw.tinyservices.id.common.entity.IdToken;

import java.util.List;

/**
 * @author TongWei.Chen 2022/3/31 19:22
 *
 * idToken DAO层
 **/
public interface IdTokenDAO extends BaseDAO<IdToken> {

    /**
     * 查找全部可用状态的token
     *
     * @return 可用状态token列表
     */
    List<IdToken> listEnabled();
}
