package com.ctw.tinyservices.id.server.lifecycle;

import com.ctw.tinyservices.id.common.lifecycle.AbstractIdLifecycle;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author TongWei.Chen 2022/4/2 18:49
 *
 * id-server 生命周期组件
 **/
public abstract class AbstractIdServerLifecycle extends AbstractIdLifecycle implements InitializingBean, DisposableBean {

    @Override
    public String name() {
        return "id_server";
    }

    @Override
    public void destroy() throws Exception {
        stop();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        init();
        start();
    }
}
