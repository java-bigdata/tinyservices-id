package com.ctw.tinyservices.id.server.segment.api;

import com.ctw.tinyservices.id.common.entity.IdSegment;
import com.ctw.tinyservices.id.core.segment.buffer.SegmentBuffer;
import com.ctw.tinyservices.id.server.api.AbstractIdApi;
import com.ctw.tinyservices.id.server.segment.handler.IdSegmentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author TongWei.Chen 2022/3/18 11:27
 *
 * 号段模式 sdk api
 **/
@RestController
@RequestMapping("/sdk/segment")
public class IdSegmentSdkApi extends AbstractIdApi {

    @Autowired
    private IdSegmentHandler idSegmentHandler;

    @GetMapping("/getSegmentBuffer/{bizType}")
    public SegmentBuffer getSegmentBuffer(@PathVariable String bizType) {
        if (! canVisit(bizType)) {
            return null;
        }
        return idSegmentHandler.getSegmentBuffer(bizType);
    }

    @PostMapping("/updateMaxIdAndGet")
    public IdSegment updateMaxIdAndGet(String bizType) {
        if (! canVisit(bizType)) {
            return null;
        }
        return idSegmentHandler.updateMaxIdAndGet(bizType);
    }

    @PostMapping("/updateMaxIdByStepAndGet")
    public IdSegment updateMaxIdByStepAndGet(String bizType, int step) {
        if (! canVisit(bizType)) {
            return null;
        }
        return idSegmentHandler.updateMaxIdByStepAndGet(bizType, step);
    }

}
