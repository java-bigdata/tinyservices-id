package com.ctw.tinyservices.id.server.snowflake.api;

import com.ctw.tinyservices.id.server.api.AbstractIdApi;
import com.ctw.tinyservices.id.server.snowflake.config.IdSnowflakeConfig;
import com.ctw.tinyservices.id.server.snowflake.factory.IdSnowflakeHolderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author TongWei.Chen 2022/3/18 11:27
 *
 * 雪花算法 sdk api
 **/
@RestController
@RequestMapping("/sdk/snowflake")
public class IdSnowflakeSdkApi extends AbstractIdApi {

    @Autowired
    private IdSnowflakeConfig idSnowflakeConfig;

    @GetMapping("/getWorkerId")
    public Integer getWorkerId() {
        return IdSnowflakeHolderFactory.getInstance().getIdSnowflakeHolder(idSnowflakeConfig).getWorkerId();
    }

    @GetMapping("/auth/{bizType}")
    public boolean auth(@PathVariable String bizType) {
        return canVisit(bizType);
    }

}
