package com.ctw.tinyservices.id.server.segment.handler;

import com.alibaba.druid.pool.DruidDataSource;
import com.ctw.tinyservices.id.common.config.DataSourceConfig;
import com.ctw.tinyservices.id.common.entity.IdSegment;
import com.ctw.tinyservices.id.common.utils.LogUtils;
import com.ctw.tinyservices.id.core.segment.buffer.SegmentBuffer;
import com.ctw.tinyservices.id.core.segment.service.IdSegmentService;
import com.ctw.tinyservices.id.server.handler.AbstractIdHandler;
import com.ctw.tinyservices.id.server.segment.dao.IdSegmentDAO;
import com.ctw.tinyservices.id.server.segment.dao.impl.IdSegmentDAOImpl;
import com.ctw.tinyservices.id.core.segment.generator.IdSegmentGenerator;
import com.ctw.tinyservices.id.server.segment.service.IdSegmentDbService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author TongWei.Chen 2022/3/8 20:54
 *
 * Id号段模式处理器
 **/
@Component
public class IdSegmentHandler extends AbstractIdHandler {
    private IdSegmentService idSegmentService;
    private IdSegmentDAO idSegmentDAO;

    // next three method for sdk
    public SegmentBuffer getSegmentBuffer(String bizType) {
        List<IdSegment> idSegmentList = idSegmentDAO.getByBizTypeAndNotInit(bizType);
        if (null == idSegmentList || idSegmentList.size() == 0) {
            LogUtils.warn(IdSegmentHandler.class, "No data found for type sdk, bizType is [{}]", bizType);
            return null;
        }
        SegmentBuffer buffer = new SegmentBuffer();
        buffer.setBizType(bizType);
        ((IdSegmentGenerator) idGenerator).fillSegment(buffer.getCurrent(), ((IdSegmentGenerator) idGenerator).fillSegmentBuffer(bizType, buffer.getCurrent()).getMaxId());
        return buffer;
    }

    public IdSegment updateMaxIdAndGet(String bizType) {
        return idSegmentService.updateMaxIdAndGet(bizType);
    }

    public IdSegment updateMaxIdByStepAndGet(String bizType, int step) {
        return idSegmentService.updateMaxIdByStepAndGet(bizType, step);
    }

    @Override
    protected void doInit() {
        LogUtils.info(IdSegmentHandler.class, "IdSegmentHandler doInit data begin...");
        // Config dataSource
        DruidDataSource druidDataSource = DataSourceConfig.getInstance().getDruidDataSource();
        idSegmentDAO = new IdSegmentDAOImpl(new JdbcTemplate(druidDataSource), new DataSourceTransactionManager(druidDataSource));
        idSegmentService = new IdSegmentDbService(idSegmentDAO);

        idGenerator = new IdSegmentGenerator(idSegmentService);
        idGenerator.init();
        LogUtils.info(IdSegmentHandler.class, "IdSegmentHandler doInit data end...");
    }

    @Override
    protected void doStart() {
        LogUtils.info(IdSegmentHandler.class, "IdSegmentHandler doStart data begin...");
        idGenerator.start();
        LogUtils.info(IdSegmentHandler.class, "IdSegmentHandler doStart data end...");
    }

    @Override
    protected void doStop() {
        LogUtils.info(IdSegmentHandler.class, "IdSegmentHandler doStop data begin...");
        idGenerator.stop();
        LogUtils.info(IdSegmentHandler.class, "IdSegmentHandler doStop data end...");
    }

}
