package com.ctw.tinyservices.id.server.segment.dao;

import com.ctw.tinyservices.id.common.dao.BaseDAO;
import com.ctw.tinyservices.id.common.entity.IdSegment;

import java.util.List;

/**
 * @author TongWei.Chen 2022/3/3 13:17
 *
 * Id号段DAO
 **/
public interface IdSegmentDAO extends BaseDAO<IdSegment> {

    IdSegment updateMaxIdAndGet(String bizType);

    IdSegment updateMaxIdByStepAndGet(String bizType, int step);

    List<IdSegment> getByBizTypeAndNotInit(String bizType);
}
