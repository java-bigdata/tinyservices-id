package com.ctw.tinyservices.id.server.segment.service;

import com.ctw.tinyservices.id.common.entity.IdSegment;
import com.ctw.tinyservices.id.server.segment.dao.IdSegmentDAO;
import com.ctw.tinyservices.id.core.segment.service.IdSegmentService;

import java.util.List;

/**
 * @author TongWei.Chen 2022/3/23 20:53
 *
 * IdSegmentDbService
 **/
public class IdSegmentDbService implements IdSegmentService {

    private IdSegmentDAO idSegmentDAO;

    public IdSegmentDbService(IdSegmentDAO idSegmentDAO) {
        this.idSegmentDAO = idSegmentDAO;
    }

    @Override
    public List<IdSegment> list() {
        return idSegmentDAO.list();
    }

    @Override
    public IdSegment updateMaxIdAndGet(String bizType) {
        return idSegmentDAO.updateMaxIdAndGet(bizType);
    }

    @Override
    public IdSegment updateMaxIdByStepAndGet(String bizType, int nextStep) {
        return idSegmentDAO.updateMaxIdByStepAndGet(bizType, nextStep);
    }

}
