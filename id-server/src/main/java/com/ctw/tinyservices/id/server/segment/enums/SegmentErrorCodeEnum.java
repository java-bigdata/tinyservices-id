package com.ctw.tinyservices.id.server.segment.enums;

import com.ctw.tinyservices.id.common.enums.code.IBasicCode;

/**
 * @author TongWei.Chen 2022/3/7 19:40
 *
 * 号段模式异常信息
 **/
public enum SegmentErrorCodeEnum implements IBasicCode {
    ID_IS_NULL(-1000, "No bizType record exists or double buffer is full")
    ;

    /**
     * 状态码
     */
    private int code;
    /**
     * 消息
     */
    private String msg;

    SegmentErrorCodeEnum(int value, String msg) {
        this.code = value;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
