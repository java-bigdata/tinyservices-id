package com.ctw.tinyservices.id.server.snowflake.config;

import com.ctw.tinyservices.id.core.snowflake.enums.IdSnowflakeHolderTypeEnum;
import com.google.common.base.Strings;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author TongWei.Chen 2022/4/11 16:59
 *
 * snowflake 配置
 **/
@Configuration
@ConfigurationProperties(prefix = "tinyservices.id.snowflake")
public class IdSnowflakeConfig {
    private String mode;
    private Integer port;
    private Long epoch;
    private String zkConnectionAddr;

    public String getMode() {
        return Strings.isNullOrEmpty(mode) ? IdSnowflakeHolderTypeEnum.LOCAL.name() : mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public Integer getPort() {
        return null == port ? 8080 : port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Long getEpoch() {
        return null == epoch ? 1648742400000L : epoch;
    }

    public void setEpoch(Long epoch) {
        this.epoch = epoch;
    }

    public String getZkConnectionAddr() {
        return Strings.isNullOrEmpty(zkConnectionAddr) ? "localhost:2181" : zkConnectionAddr;
    }

    public void setZkConnectionAddr(String zkConnectionAddr) {
        this.zkConnectionAddr = zkConnectionAddr;
    }
}
