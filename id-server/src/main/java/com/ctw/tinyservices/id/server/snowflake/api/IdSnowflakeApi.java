package com.ctw.tinyservices.id.server.snowflake.api;

import com.ctw.tinyservices.id.common.dto.ResponseDTO;
import com.ctw.tinyservices.id.common.dto.helper.ResponseDTOHelper;
import com.ctw.tinyservices.id.common.enums.code.ErrorCodeEnum;
import com.ctw.tinyservices.id.server.api.AbstractIdApi;
import com.ctw.tinyservices.id.server.snowflake.enums.SnowflakeErrorCodeEnum;
import com.ctw.tinyservices.id.server.snowflake.handler.IdSnowflakeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author TongWei.Chen 2022/4/14 19:27
 *
 * 雪花算法 api
 **/
@RestController
@RequestMapping("/snowflake")
public class IdSnowflakeApi extends AbstractIdApi {

    @Autowired
    private IdSnowflakeHandler idSnowflakeHandler;

    @GetMapping("/{bizType}")
    public ResponseDTO<Long> get(@PathVariable String bizType) {
        if (! canVisit(bizType)) {
            return ResponseDTOHelper.fail(ErrorCodeEnum.AUTH_ERROR);
        }
        try {
            return ResponseDTOHelper.success(idSnowflakeHandler.getId(bizType));
        } catch (Exception e) {
            return ResponseDTOHelper.fail(SnowflakeErrorCodeEnum.ERROR);
        }
    }

    @GetMapping("/{bizType}/{num}")
    public ResponseDTO<List<Long>> get(@PathVariable("bizType") String bizType, @PathVariable("num") int num) {
        return batchId(bizType, num);
    }

    @Override
    protected List<Long> doBatchId(String bizType, int num) {
        return idSnowflakeHandler.listId(bizType, num);
    }
}
