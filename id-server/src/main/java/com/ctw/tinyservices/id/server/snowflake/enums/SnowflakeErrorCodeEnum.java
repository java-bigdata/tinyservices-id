package com.ctw.tinyservices.id.server.snowflake.enums;

import com.ctw.tinyservices.id.common.enums.code.IBasicCode;

/**
 * @author TongWei.Chen 2022/3/7 19:40
 *
 * 雪花算法模式异常信息
 **/
public enum SnowflakeErrorCodeEnum implements IBasicCode {
    ERROR(-2000, "It may be that the system time has changed or the initialization failed, please check the server log"),
    ;

    /**
     * 状态码
     */
    private int code;
    /**
     * 消息
     */
    private String msg;

    SnowflakeErrorCodeEnum(int value, String msg) {
        this.code = value;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
