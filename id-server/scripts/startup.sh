#!/usr/bin/env bash

# 程序执行的根路径
PROJECT_NAME=id-server
PROJ_DIR=${PROJECT_NAME}/target
# 程序目标
PROJ_TARGET_JAR=${PROJ_DIR}/${PROJECT_NAME}.jar
JAVA_CMD=java
JVM_ARGS="-jar -Xmx4096M -Xms4096M -XX:MaxMetaspaceSize=512M -XX:MetaspaceSize=512M -XX:+UseG1GC -XX:MaxGCPauseMillis=100 -XX:+ParallelRefProcEnabled -Dname=$PROJECT_NAME"

CMD="${JAVA_CMD} ${JVM_ARGS}  ${PROJ_TARGET_JAR}"
echo "id-server Start--------------"
echo "JVM ARGS   ${JVM_ARGS}"
echo "->"
echo "PROJ_TARGET_JAR ${PROJ_TARGET_JAR}"
echo "------------------------"
$CMD
